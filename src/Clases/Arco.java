package Clases;

public class Arco<K> implements Comparable<Arco<K>>{

	// ---------------------------------------------------
	// --------------------Atributos----------------------
	// ---------------------------------------------------

	private char identificadorLexicografico;
	private double peso;
	private K llaveDestino;

	// ---------------------------------------------------
	// --------------------Constructor--------------------
	// ---------------------------------------------------

	public Arco() {
		peso = 0;
		identificadorLexicografico = '+';
		llaveDestino = null;
	}

	// ---------------------------------------------------
	// --------------------Metodos------------------------
	// ---------------------------------------------------

	public K getLlaveDestino() {
		return llaveDestino;
	}

	public void setLlaveDestino(K llaveDestino) {
		this.llaveDestino = llaveDestino;
	}

	public char getIdentificadorLexicografico() {
		return identificadorLexicografico;
	}

	public void setIdentificadorLexicografico(char identificadorLexicografico) {
		this.identificadorLexicografico = identificadorLexicografico;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public String toString() {
		return "Arco [identificadorLexicografico=" + identificadorLexicografico + ", peso=" + peso + ", llaveDestino="
				+ llaveDestino + "]";
	}
	
	public int compareTo(Arco a){
		if(identificadorLexicografico - a.identificadorLexicografico < 0){
			return 1;
		}
		else if(identificadorLexicografico - a.identificadorLexicografico > 0){
			return -1;
		}
		return 0;
	}



}
