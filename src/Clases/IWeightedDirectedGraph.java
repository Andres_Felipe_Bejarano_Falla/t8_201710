package Clases;

import java.util.Iterator;

import model.data_structures.ListaEncadenada;

public interface IWeightedDirectedGraph<K, V> {
	/**
	 * Retornar el n�mero de v�rtices
	 * 
	 * @return
	 */
	public int numVertices();

	/**
	 * Consultar la informaci�n de un v�rtice dado su identificador. Si el
	 * identificador no existe se retorna la excepci�n �NoSuchElementException�.
	 * 
	 * @return
	 */
	public V darVertice(K id);

	/**
	 * Agregar un v�rtice con identificador �nico y su informaci�n asociada. Si
	 * el identificador ya existe, se reemplaza la informaci�n asociada al
	 * v�rtice
	 * 
	 * @return
	 */
	public void agregarVertice(K id, V infoVer);

	/**
	 * Retornar el n�mero de arcos
	 * 
	 * @return
	 */
	public int numArcos();

	/**
	 * Consultar el peso/costo del arco entre un v�rtice origen y un v�rtice
	 * destino. Se supone que m�ximo puede existir un arco entre un v�rtice
	 * origen y un v�rtice destino. Si no existe un arco en la direcci�n dada
	 * por los v�rtices, se retorna la excepci�n �NoSuchElementException�
	 * 
	 * @return
	 */
	public double darPesoArco(K idOrigen, K idDestino);

	/**
	 * Agregar un arco entre un v�rtice origen y un v�rtice destino. El arco que
	 * conecta el v�rtice origen y el v�rtice destino tiene asociado un
	 * peso/costo (valor positivo). El nuevo arco se agrega al final de los
	 * arcos asociados al v�rtice idOrigen. Se supone que m�ximo puede existir
	 * un arco entre un v�rtice origen y un v�rtice destino. Si el arco ya
	 * existe, se reemplaza su peso/costo (valor positivo).
	 * 
	 * @return
	 */
	public void agregarArco(K idOrigen, K idDestino, double peso);

	/**
	 * Agregar un arco entre un v�rtice origen y un v�rtice destino respetando
	 * un orden lexicogr�fico en el v�rtice origen. El arco que conecta el
	 * v�rtice origen y el v�rtice destino tiene asociado un peso/costo (valor
	 * positivo).
	 * 
	 * Aclaraci�n: Si el v�rtice idOrigen tiene dos arcos uno con orden
	 * lexicogr�fico 'a' y otro con 'b', el v�rtice idOrigen debe garantizar que
	 * su primer arco sea el que tenga el orden lexicogr�fico 'a' y el segundo
	 * arco el que tenga 'b'.
	 * 
	 * @return
	 */
	public void agregarArco(K idOrigen, K idDestino, double peso, char ordenLexicografico);

	/**
	 * Retornar el conjunto de los identificadores de v�rtices en el grafo
	 * 
	 * @return
	 */
	public Iterator<K> darVertices();

	/**
	 * Dar el grado de un v�rtice. El grado se define como su n�mero de v�rtices
	 * adyacentes. Los v�rtices adyacentes son aquellos con los cuales se tiene
	 * un arco saliendo del v�rtice.
	 * 
	 * @return
	 */
	public int darGrado(K id);

	/**
	 * Retornar el conjunto de v�rtices adyacentes de un v�rtice.
	 * 
	 * @return
	 */
	public Iterator<K> darVerticesAdacentes(K id);

	/**
	 * Retornar el conjunto de v�rtices accesibles desde un v�rtice origen
	 * usando el recorrido Depth-First Search. La respuesta es el conjunto de
	 * v�rtices iniciando desde idOrigen y representados por un arreglo de
	 * objetos NodoCamino<K>. Note que el arreglo NodoCamino<K>[] contiene el
	 * conjunto de v�rtices tal cual como es generado por un algoritmo de
	 * recorrido en grafos (ej., DFS o BFS).
	 * 
	 * @return
	 */
	public ListaEncadenada<NodoCamino<K>> DFS(K idOrigen);

	/**
	 * Retornar el conjunto de v�rtices accesibles desde un v�rtice (idOrigen)
	 * origen usando el recorrido Breadth-First Search. La respuesta es el
	 * conjunto de v�rtices iniciando desde idOrigen. La clase NodoCamino hace
	 * referencia a la misma clase del m�todo DFS().
	 * 
	 * @return
	 */
	public ListaEncadenada<NodoCamino<K>> BFS(K idOrigen);

	/**
	 * Retornar el camino (secuencia de v�rtices) desde el v�rtice idOrigen al
	 * v�rtice idDestino usando un recorrido DFS. Si no existe un camino se debe
	 * retornar el valor null. La clase NodoCamino hace referencia a la misma
	 * clase del m�todo DFS(). 
	 * 
	 * Ayuda: se puede invocar el m�todo DFS(K idOrigen)
	 * 
	 * @return
	 */
	public ListaEncadenada<NodoCamino<K>> DarCaminoDFS(K idOrigen, K idDestino);

	/**
	 * Retornar el camino (secuencia de v�rtices) desde el v�rtice idOrigen al
	 * v�rtice idDestino usando un recorrido BFS. Si no existe un camino se debe
	 * retornar el valor null. La clase NodoCamino hace referencia a la misma
	 * clase del m�todo DFS(). 
	 * 
	 * Ayuda: se puede invocar el m�todo BFS(K idOrigen)
	 * 
	 * @return
	 */
	public ListaEncadenada<NodoCamino<K>> DarCaminoBFS(K idOrigen, K idDestino);
}
