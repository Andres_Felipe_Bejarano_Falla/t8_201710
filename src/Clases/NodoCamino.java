package Clases;

public class NodoCamino<K> {

	// ---------------------------------------------------
	// --------------------Atributos----------------------
	// ---------------------------------------------------

	private K idOrigen;
	private K idAdy;
	private K idFinal;
	private double peso;
	private int longitud;

	// ---------------------------------------------------
	// --------------------Constructor--------------------
	// ---------------------------------------------------

	public NodoCamino() {
		idFinal = null;
		idOrigen = null;
		idAdy = null;
		peso = 0.0;
		longitud = 0;
	}

	// ---------------------------------------------------
	// --------------------Metodos------------------------
	// ---------------------------------------------------

	public K getIdFinal() {
		return idFinal;
	}

	public void setIdFinal(K idFinal) {
		this.idFinal = idFinal;
	}

	public K getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(K idOrigen) {
		this.idOrigen = idOrigen;
	}

	public K getIdAdy() {
		return idAdy;
	}

	public void setIdAdy(K idAdy) {
		this.idAdy = idAdy;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public String toString() {
		return "NodoCamino [idIntermedio=" + idAdy +  ", idFinal=" + idFinal + ", idOrigen=" + idOrigen +", peso="
				+ peso + ", longitud=" + longitud + "]";
	}

}
