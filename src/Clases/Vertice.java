package Clases;

import model.data_structures.ColaPrioridad;
import model.data_structures.EncadenamientoSeparadoTH;

public class Vertice<K, V> {

	// ---------------------------------------------------
	// --------------------Atributos----------------------
	// ---------------------------------------------------

	private V valor;
	private boolean marked;
	private K llave;
	private EncadenamientoSeparadoTH<K, Arco<K>> enlaces;
	private ColaPrioridad<Arco<K>> arcosOrdenados;

	// ---------------------------------------------------
	// --------------------Constructor--------------------
	// ---------------------------------------------------

	public Vertice() {
		valor = null;
		marked = false;
		llave = null;
		enlaces = null;
		arcosOrdenados = new ColaPrioridad<>();
	}

	// ---------------------------------------------------
	// --------------------Metodos------------------------
	// ---------------------------------------------------

	public ColaPrioridad<Arco<K>> getArcosOrdenados() {
		arcosOrdenados.crearCP(enlaces.darTamanio());
		for(K llave : enlaces.llaves()){
			try {
				arcosOrdenados.agregar(enlaces.darValor(llave));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return arcosOrdenados;
	}

	public V getValor() {
		return valor;
	}

	public void setValor(V valor) {
		this.valor = valor;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}

	public K getLlave() {
		return llave;
	}

	public void setLlave(K llave) {
		this.llave = llave;
	}

	public EncadenamientoSeparadoTH<K, Arco<K>> getEnlaces() {
		return enlaces;
	}

	public int darCantidadEnlaces() {
		return getEnlaces().darTamanio();
	}

	public void setEnlaces(EncadenamientoSeparadoTH<K, Arco<K>> enlaces) {
		this.enlaces = enlaces;
	}

	public String toString() {
		return "Vertice [valor=" + valor + ", marked=" + marked + ", llave=" + llave + "]";
	}

}
