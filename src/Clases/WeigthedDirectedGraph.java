package Clases;

import java.util.Iterator;
import java.util.NoSuchElementException;

import model.data_structures.ColaPrioridad;
import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.ListaEncadenada;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class WeigthedDirectedGraph<K, V> implements IWeightedDirectedGraph<K, V> {

	private EncadenamientoSeparadoTH<K, Vertice<K, V>> tablaDeVertices;
	private Queue<K> pre;
	private Queue<K> post;
	private Stack<K> revertPost;
	private EncadenamientoSeparadoTH<K, K> edgeTo;

	public WeigthedDirectedGraph() {
		tablaDeVertices = new EncadenamientoSeparadoTH<>(75);
	}

	public int numVertices() {
		return tablaDeVertices.darTamanio();
	}

	public V darVertice(K id) {
		return tablaDeVertices.darValor(id).getValor();
	}

	public void agregarVertice(K id, V infoVer) {
		Vertice<K, V> nuevoVertice = new Vertice<>();
		nuevoVertice.setLlave(id);
		nuevoVertice.setValor(infoVer);
		nuevoVertice.setEnlaces(new EncadenamientoSeparadoTH<K, Arco<K>>(15));
		tablaDeVertices.insertar(id, nuevoVertice);
	}

	public int numArcos() {
		int numeroFinal = 0;
		for (K iterable_element : tablaDeVertices.llaves()) {
			numeroFinal += tablaDeVertices.darValor(iterable_element).darCantidadEnlaces();
		}
		return numeroFinal;
	}

	public double darPesoArco(K idOrigen, K idDestino) throws NoSuchElementException {
		Arco arco = tablaDeVertices.darValor(idOrigen).getEnlaces().darValor(idDestino);
		if (arco == null)
			throw new NoSuchElementException();
		else
			return arco.getPeso();
	}

	public void agregarArco(K idOrigen, K idDestino, double peso) {

		Arco arco = new Arco();
		arco.setPeso(peso);

		int mayorcant = -1;

		Arco pArco = tablaDeVertices.darValor(idOrigen).getEnlaces().darValor(idDestino);

		if (pArco == null) {
			for (K llave : tablaDeVertices.darValor(idOrigen).getEnlaces().llaves()) {
				int g = tablaDeVertices.darValor(idOrigen).getEnlaces().darValor(llave).getIdentificadorLexicografico()
						- 'A';
				if (g > mayorcant)
					mayorcant = g;
			}
			mayorcant += 1;
			char t = (char) ('A' + mayorcant);
			arco.setIdentificadorLexicografico(t);
			arco.setLlaveDestino(idDestino);
			tablaDeVertices.darValor(idOrigen).getEnlaces().insertar(idDestino, arco);
		} else
			tablaDeVertices.darValor(idOrigen).getEnlaces().darValor(idDestino).setPeso(peso);
	}

	public void agregarArco(K idOrigen, K idDestino, double peso, char ordenLexicografico) {
		Arco arco = new Arco();
		arco.setPeso(peso);
		arco.setIdentificadorLexicografico(ordenLexicografico);
		arco.setLlaveDestino(idDestino);
		Arco pArco = tablaDeVertices.darValor(idOrigen).getEnlaces().darValor(idDestino);
		if (pArco == null) {
			tablaDeVertices.darValor(idOrigen).getEnlaces().insertar(idDestino, arco);
		} else {
			tablaDeVertices.darValor(idOrigen).getEnlaces().darValor(idDestino).setPeso(peso);
			tablaDeVertices.darValor(idOrigen).getEnlaces().darValor(idDestino)
					.setIdentificadorLexicografico(ordenLexicografico);
		}
	}

	public Iterator<K> darVertices() {
		return tablaDeVertices.llaves().iterator();
	}

	public int darGrado(K id) {
		return tablaDeVertices.darValor(id).getEnlaces().darTamanio();
	}

	public Iterator<K> darVerticesAdacentes(K id) {
		return tablaDeVertices.darValor(id).getEnlaces().llaves().iterator();
	}

	public void desmarcar() {
		for (K llave : tablaDeVertices.llaves()) {
			tablaDeVertices.darValor(llave).setMarked(false);
		}
	}

	public ListaEncadenada<NodoCamino<K>> DFS(K idOrigen) {
		desmarcar();
		ListaEncadenada<NodoCamino<K>> camino = new ListaEncadenada<>();
		pre = new Queue<K>();
		post = new Queue<K>();
		revertPost = new Stack<K>();

		dfs(idOrigen, idOrigen, camino, 0, 0);

		// NodoCamino<K> inicio = camino.darElemento(0);
		// camino.darElemento(0).setLongitud(1);
		// camino.darElemento(0).setPeso(darPesoArco(inicio.getIdAdy(),
		// inicio.getIdFinal()));
		// for(int i =1;i<camino.darNumeroElementos();i++){
		// NodoCamino<K> cActual = camino.darElemento(i);
		// for(int j = 0;j<camino.darNumeroElementos();j++){
		// NodoCamino<K> cAnterior = camino.darElemento(i);
		// if(cAnterior.getIdFinal().equals(cActual.getIdAdy())){
		// System.out.println("entro");
		// cActual.setLongitud(cAnterior.getLongitud()+1);
		// cActual.setPeso(cAnterior.getPeso()+ darPesoArco(cActual.getIdAdy(),
		// cActual.getIdFinal()));
		// break;
		// }
		// }
		// }
		return camino;
	}

	private void dfs(K v, K origen, ListaEncadenada<NodoCamino<K>> camino, int longitud, double peso) {
		pre.enqueue(v);
		tablaDeVertices.darValor(v).setMarked(true);
		Vertice<K, V> verticeActual = null;
		ColaPrioridad<Arco<K>> arcos = tablaDeVertices.darValor(v).getArcosOrdenados();
		while (!arcos.esVacia()) {
			K llave = arcos.max().getLlaveDestino();
			verticeActual = tablaDeVertices.darValor(llave);
			if (!verticeActual.isMarked()) {
				NodoCamino<K> paso = new NodoCamino<>();
				paso.setIdOrigen(origen);
				paso.setIdAdy(v);
				paso.setIdFinal(llave);
				int newLong = longitud + 1;
				double newPeso = peso + darPesoArco(v, llave);
				paso.setLongitud(newLong);
				paso.setPeso(newPeso);
				camino.agregarElementoFinal(paso);
				dfs(llave, origen, camino, newLong, newPeso);
			}
		}
		post.enqueue(v);
		revertPost.push(v);
	}

	public ListaEncadenada<NodoCamino<K>> BFS(K idOrigen) {
		desmarcar();
		ListaEncadenada<NodoCamino<K>> camino = new ListaEncadenada<>();
		edgeTo = new EncadenamientoSeparadoTH<>(numVertices());
		bfs(idOrigen, idOrigen, camino);
		return camino;
	}

	private void bfs(K inicio, K origen, ListaEncadenada<NodoCamino<K>> camino) {
		Queue<K> colaTrabajo = new Queue<>();
		tablaDeVertices.darValor(inicio).setMarked(true);
		colaTrabajo.enqueue(inicio);
		while (!colaTrabajo.isEmpty()) {
			Vertice<K, V> verticeActual = null;
			K v = colaTrabajo.dequeue();
			ColaPrioridad<Arco<K>> arcos = tablaDeVertices.darValor(v).getArcosOrdenados();
			while (!arcos.esVacia()) {
				K w = arcos.max().getLlaveDestino();
				verticeActual = tablaDeVertices.darValor(w);
				if (!verticeActual.isMarked()) {
					NodoCamino<K> paso = new NodoCamino<>();
					paso.setIdOrigen(origen);
					paso.setIdAdy(v);
					paso.setIdFinal(w);
					paso.setLongitud(1);
					paso.setPeso(darPesoArco(v, w));
					camino.agregarElementoFinal(paso);
					edgeTo.insertar(w, v);
					tablaDeVertices.darValor(w).setMarked(true);
					colaTrabajo.enqueue(w);
				}
			}
		}
	}

	public ListaEncadenada<NodoCamino<K>> DarCaminoDFS(K idOrigen, K idDestino) {
		// TODO Auto-generated method stub
		ListaEncadenada<NodoCamino<K>> camino = DFS(idOrigen);
		ListaEncadenada<NodoCamino<K>> caminoRespuesta = new ListaEncadenada<>();
		K temp = idDestino;
		boolean encontro = false;
		for (int i = 0; i < camino.darNumeroElementos() && !encontro; i++) {
			for (int j = 0; j < camino.darNumeroElementos(); j++) {
				if (camino.darElemento(j).getIdFinal().equals(temp)) {
					caminoRespuesta.agregarALInicio(camino.darElemento(j));
					temp = camino.darElemento(j).getIdAdy();
					if (temp.equals(idOrigen))
						encontro = true;
					break;
				}
			}
		}

		for (NodoCamino<K> c : caminoRespuesta) {
			c.setLongitud(caminoRespuesta.darElemento(caminoRespuesta.darNumeroElementos() - 1).getLongitud());
			c.setPeso(caminoRespuesta.darElemento(caminoRespuesta.darNumeroElementos() - 1).getPeso());
			c.setIdFinal(caminoRespuesta.darElemento(caminoRespuesta.darNumeroElementos() - 1).getIdFinal());
		}

		return caminoRespuesta;

	}

	public ListaEncadenada<NodoCamino<K>> DarCaminoBFS(K idOrigen, K idDestino) {
		// TODO Auto-generated method stub
		ListaEncadenada<NodoCamino<K>> camino = BFS(idOrigen);
		ListaEncadenada<NodoCamino<K>> caminoRespuesta = new ListaEncadenada<>();
		K temp = idDestino;
		boolean encontro = false;
		for (int i = 0; i < camino.darNumeroElementos() && !encontro; i++) {
			for (int j = 0; j < camino.darNumeroElementos(); j++) {
				if (camino.darElemento(j).getIdFinal().equals(temp)) {
					caminoRespuesta.agregarALInicio(camino.darElemento(j));
					temp = camino.darElemento(j).getIdAdy();
					if (temp.equals(idOrigen))
						encontro = true;
					break;
				}
			}
		}

		int longitud = 0;
		double peso = 0.0;
		for (NodoCamino<K> c : caminoRespuesta) {
			longitud += c.getLongitud();
			peso += c.getPeso();
		}

		for (NodoCamino<K> c : caminoRespuesta) {
			c.setLongitud(longitud);
			c.setPeso(peso);
			c.setIdFinal(idDestino);
		}

		return caminoRespuesta;
	}

}
