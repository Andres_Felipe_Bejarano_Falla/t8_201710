package model.data_structures;
/*  Sacado de: algs4.cs.princeton.edu/32bst/BST.java.html
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */

public class BinarySearchTree<K extends Comparable<K>, V> {

	private Node root;

	private class Node {
		private K key;
		private V val;
		private Node left, right;
		private int size;

		public Node(K key, V val, int size) {
			this.key = key;
			this.val = val;
			this.size = size;
		}
	}

	public BinarySearchTree() {
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public int size() {
		return size(root);
	}

	private int size(Node n) {
		if (n == null)
			return 0;
		else
			return n.size;
	}

	public V get(K key) throws Exception {
		return get(root, key);
	}

	private V get(Node n, K llave) throws Exception {
		if (llave == null)
			throw new Exception("Llave nula.");
		if (n == null)
			return null;
		int cmp = llave.compareTo(n.key);
		if (cmp < 0)
			return get(n.left, llave);
		else if (cmp > 0)
			return get(n.right, llave);
		else
			return n.val;
	}

	public void put(K llave, V value) throws Exception {
		if (llave == null)
			throw new Exception("Llave nula.");
		if (value == null) {
			delete(llave);
			return;
		}
		root = put(root, llave, value);
	}

	private Node put(Node x, K llave, V value) {
		if (x == null)
			return new Node(llave, value, 1);
		int cmp = llave.compareTo(x.key);
		if (cmp < 0)
			x.left = put(x.left, llave, value);
		else if (cmp > 0)
			x.right = put(x.right, llave, value);
		else
			x.val = value;
		x.size = 1 + size(x.left) + size(x.right);
		return x;
	}

	public void delete(K key) throws Exception {
		if (key == null)
			throw new Exception("Llave nula.");
		root = delete(root, key);
	}

	private Node delete(Node x, K key) {
		if (x == null)
			return null;

		int cmp = key.compareTo(x.key);
		if (cmp < 0)
			x.left = delete(x.left, key);
		else if (cmp > 0)
			x.right = delete(x.right, key);
		else {
			if (x.right == null)
				return x.left;
			if (x.left == null)
				return x.right;
			Node t = x;
			x = min(t.right);
			x.right = deleteMin(t.right);
			x.left = t.left;
		}
		x.size = size(x.left) + size(x.right) + 1;
		return x;
	}

	public void deleteMin() throws Exception {
		if (isEmpty())
			throw new Exception("Holi");
		root = deleteMin(root);
	}

	private Node deleteMin(Node x) {
		if (x.left == null)
			return x.right;
		x.left = deleteMin(x.left);
		x.size = size(x.left) + size(x.right) + 1;
		return x;
	}

	public K min() throws Exception {
		if (isEmpty())
			throw new Exception("tabla vacia");
		return min(root).key;
	}

	private Node min(Node x) {
		if (x.left == null)
			return x;
		else
			return min(x.left);
	}

	public int height() {
		return height(root);
	}

	private int height(Node x) {
		if (x == null)
			return -1;
		return 1 + Math.max(height(x.left), height(x.right));
	}
}