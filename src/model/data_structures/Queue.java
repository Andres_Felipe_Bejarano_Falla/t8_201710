package model.data_structures;

import java.util.Iterator;

public class Queue <T>implements IQueue<T> , Iterable<T>{

	private ListaEncadenada<T> lista;

	public Queue() {
		lista = new ListaEncadenada<>();
	}

	public void enqueue(T elemento) {
		lista.agregarElementoFinal(elemento);
	}

	public T dequeue() {
		T e =  lista.darElemento(0);
		lista.removeInicial();
		return e;
	}

	@Override
	public String toString() {
		return "Queue [lista=" + lista + "]";
	}

	public int size() {
		return lista.darNumeroElementos();
	}

	public boolean isEmpty() {
		return lista.darNumeroElementos()==0;
	}

	public Iterator<T> iterator() {
		return lista.iterator();	
	}

}
