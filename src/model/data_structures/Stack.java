package model.data_structures;

public class Stack <T> implements IStack<T>{

	private ListaEncadenada<T> lista;
	
	public Stack() {
		lista = new ListaEncadenada<T>();
	}

	@Override
	public void push(T elemento) {
		// TODO Auto-generated method stub
		lista.agregarALInicio(elemento);
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		T e = lista.darElemento(0);
		lista.removeInicial();
		return e;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos()==0;
	}
	
}
