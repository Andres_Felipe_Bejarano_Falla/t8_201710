package test;

import Clases.NodoCamino;
import Clases.WeigthedDirectedGraph;
import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class WeightedDirectedGraphTest extends TestCase {

 WeigthedDirectedGraph<Integer, Integer> grafoPrueba;

 public void setupEscenario1() {
  grafoPrueba = new WeigthedDirectedGraph<>();
  for (int i = 0; i < 9; i++) {
   grafoPrueba.agregarVertice(i + 1, i + 1);
  }
  grafoPrueba.agregarArco(1, 5, 4);
  grafoPrueba.agregarArco(2, 9, 4);
  grafoPrueba.agregarArco(2, 4, 1);
  grafoPrueba.agregarArco(3, 1, 2);
  grafoPrueba.agregarArco(3, 5, 4);
  grafoPrueba.agregarArco(4, 8, 3);
  grafoPrueba.agregarArco(5, 7, 5);
  grafoPrueba.agregarArco(5, 2, 8);
  grafoPrueba.agregarArco(5, 4, 8);
  grafoPrueba.agregarArco(5, 6, 5);
  grafoPrueba.agregarArco(6, 4, 4);
  grafoPrueba.agregarArco(6, 8, 6);
  grafoPrueba.agregarArco(6, 3, 6);
  grafoPrueba.agregarArco(7, 9, 6);
  grafoPrueba.agregarArco(7, 2, 1);
  grafoPrueba.agregarArco(7, 1, 10);
  grafoPrueba.agregarArco(8, 3, 4);
  grafoPrueba.agregarArco(9, 1, 4);
 }

 public void testNumArcos() {
  setupEscenario1();
  assertEquals(18, grafoPrueba.numArcos());
 }

 public void testNumVertices() {
  setupEscenario1();
  assertEquals(18 / 2, grafoPrueba.numVertices());
 }

 public void testDarVertice() {
  setupEscenario1();
  try {
   grafoPrueba.darVertice(-1);
   fail();
  } catch (Exception e) {
  }
  for (int i = 0; i < 9; i++) {
   assertEquals(new Integer(i + 1), grafoPrueba.darVertice(i + 1));
  }
 }

 public void testDarPesoArco() {
  setupEscenario1();
  assertEquals(10.0, grafoPrueba.darPesoArco(7, 1));
  assertEquals(5.0, grafoPrueba.darPesoArco(5, 7));
  assertEquals(6.0, grafoPrueba.darPesoArco(6, 3));
  assertEquals(1.0, grafoPrueba.darPesoArco(2, 4));
  assertEquals(4.0, grafoPrueba.darPesoArco(2, 9));
 }

 public void testDarGrado() {
  setupEscenario1();
  assertEquals(1, grafoPrueba.darGrado(1));
  assertEquals(1, grafoPrueba.darGrado(4));
  assertEquals(2, grafoPrueba.darGrado(2));
  assertEquals(2, grafoPrueba.darGrado(3));
  assertEquals(4, grafoPrueba.darGrado(5));
  assertEquals(3, grafoPrueba.darGrado(6));
  assertEquals(3, grafoPrueba.darGrado(7));
  assertEquals(1, grafoPrueba.darGrado(8));
  assertEquals(1, grafoPrueba.darGrado(9));
 }
 
 public void testDarCaminoDFS1(){
	 
	 setupEscenario1();
	 
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoDFS(1, 8);
	 
	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(1));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(1));
	 assertEquals(camino.darElemento(0).getPeso(), 14.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 5);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(1));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(5));
	 assertEquals(camino.darElemento(1).getPeso(), 14.0);
	 assertEquals(camino.darElemento(1).getLongitud(), 5);
	 
	 assertEquals(camino.darElemento(2).getIdOrigen(), new Integer(1));
	 assertEquals(camino.darElemento(2).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(2).getIdAdy(), new Integer(7));
	 assertEquals(camino.darElemento(2).getPeso(), 14.0);
	 assertEquals(camino.darElemento(2).getLongitud(), 5);
	 
	 assertEquals(camino.darElemento(3).getIdOrigen(), new Integer(1));
	 assertEquals(camino.darElemento(3).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(3).getIdAdy(), new Integer(2));
	 assertEquals(camino.darElemento(3).getPeso(), 14.0);
	 assertEquals(camino.darElemento(3).getLongitud(), 5);
	 
	 assertEquals(camino.darElemento(4).getIdOrigen(), new Integer(1));
	 assertEquals(camino.darElemento(4).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(4).getIdAdy(), new Integer(4));
	 assertEquals(camino.darElemento(4).getPeso(), 14.0);
	 assertEquals(camino.darElemento(4).getLongitud(), 5);

 }
 
 public void testDarCaminoDFS2(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoDFS(5, 9);
	 
	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(5));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(9));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(5));
	 assertEquals(camino.darElemento(0).getPeso(), 11.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 2);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(5));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(9));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(7));
	 assertEquals(camino.darElemento(1).getPeso(), 11.0);
	 assertEquals(camino.darElemento(1).getLongitud(), 2);
 }

 public void testDarCaminoDFS3(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoDFS(6, 8);
	 
	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(6));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(6));
	 assertEquals(camino.darElemento(0).getPeso(), 7.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 2);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(6));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(4));
	 assertEquals(camino.darElemento(1).getPeso(), 7.0);
	 assertEquals(camino.darElemento(1
			 ).getLongitud(), 2);
 }
 
 public void testDarCaminoDFS4(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoDFS(7, 5);

	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(7));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(5));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(7));
	 assertEquals(camino.darElemento(0).getPeso(), 14.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 3);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(7));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(5));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(9));
	 assertEquals(camino.darElemento(1).getPeso(), 14.0);
	 assertEquals(camino.darElemento(1).getLongitud(), 3);
	 
	 assertEquals(camino.darElemento(2).getIdOrigen(), new Integer(7));
	 assertEquals(camino.darElemento(2).getIdFinal(), new Integer(5));
	 assertEquals(camino.darElemento(2).getIdAdy(), new Integer(1));
	 assertEquals(camino.darElemento(2).getPeso(), 14.0);
	 assertEquals(camino.darElemento(2).getLongitud(), 3);
 }
 
 public void testDarCaminoDFS5(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoDFS(9, 3);

	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(9));
	 assertEquals(camino.darElemento(0).getPeso(), 22.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 7);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(1));
	 assertEquals(camino.darElemento(1).getPeso(), 22.0);
	 assertEquals(camino.darElemento(1).getLongitud(), 7);
	
	 assertEquals(camino.darElemento(2).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(2).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(2).getIdAdy(), new Integer(5));
	 assertEquals(camino.darElemento(2).getPeso(), 22.0);
	 assertEquals(camino.darElemento(2).getLongitud(), 7);
	 
	 assertEquals(camino.darElemento(3).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(3).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(3).getIdAdy(), new Integer(7));
	 assertEquals(camino.darElemento(3).getPeso(), 22.0);
	 assertEquals(camino.darElemento(3).getLongitud(), 7);
	 
	 assertEquals(camino.darElemento(4).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(4).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(4).getIdAdy(), new Integer(2));
	 assertEquals(camino.darElemento(4).getPeso(), 22.0);
	 assertEquals(camino.darElemento(4).getLongitud(), 7);

	 assertEquals(camino.darElemento(5).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(5).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(5).getIdAdy(), new Integer(4));
	 assertEquals(camino.darElemento(5).getPeso(), 22.0);
	 assertEquals(camino.darElemento(5).getLongitud(), 7);
	 
	 assertEquals(camino.darElemento(6).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(6).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(6).getIdAdy(), new Integer(8));
	 assertEquals(camino.darElemento(6).getPeso(), 22.0);
	 assertEquals(camino.darElemento(6).getLongitud(), 7);
 }
 
 public void testDarCaminoBFS1(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoBFS(9, 3);
	 
	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(9));
	 assertEquals(camino.darElemento(0).getPeso(), 19.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 4);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(1));
	 assertEquals(camino.darElemento(1).getPeso(), 19.0);
	 assertEquals(camino.darElemento(1).getLongitud(), 4);
	 
	 assertEquals(camino.darElemento(2).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(2).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(2).getIdAdy(), new Integer(5));
	 assertEquals(camino.darElemento(2).getPeso(), 19.0);
	 assertEquals(camino.darElemento(2).getLongitud(), 4);
	 
	 assertEquals(camino.darElemento(3).getIdOrigen(), new Integer(9));
	 assertEquals(camino.darElemento(3).getIdFinal(), new Integer(3));
	 assertEquals(camino.darElemento(3).getIdAdy(), new Integer(6));
	 assertEquals(camino.darElemento(3).getPeso(), 19.0);
	 assertEquals(camino.darElemento(3).getLongitud(), 4);
	 

	 
 }
 public void testDarCaminoBFS2(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoBFS(3, 5);
	 
	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(3));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(5));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(3));
	 assertEquals(camino.darElemento(0).getPeso(), 4.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 1);
	 
 }
 public void testDarCaminoBFS3(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoBFS(8, 7);
	 
	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(8));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(7));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(8));
	 assertEquals(camino.darElemento(0).getPeso(), 13.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 3);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(8));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(7));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(3));
	 assertEquals(camino.darElemento(1).getPeso(), 13.0);
	 assertEquals(camino.darElemento(1).getLongitud(), 3);
	 
	 assertEquals(camino.darElemento(2).getIdOrigen(), new Integer(8));
	 assertEquals(camino.darElemento(2).getIdFinal(), new Integer(7));
	 assertEquals(camino.darElemento(2).getIdAdy(), new Integer(5));
	 assertEquals(camino.darElemento(2).getPeso(), 13.0);
	 assertEquals(camino.darElemento(2).getLongitud(), 3);
	 
 }
 public void testDarCaminoBFS4(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoBFS(1, 8);
	 
	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(1));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(1));
	 assertEquals(camino.darElemento(0).getPeso(), 15.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 3);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(1));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(5));
	 assertEquals(camino.darElemento(1).getPeso(), 15.0);
	 assertEquals(camino.darElemento(1).getLongitud(), 3);
	 
	 assertEquals(camino.darElemento(2).getIdOrigen(), new Integer(1));
	 assertEquals(camino.darElemento(2).getIdFinal(), new Integer(8));
	 assertEquals(camino.darElemento(2).getIdAdy(), new Integer(4));
	 assertEquals(camino.darElemento(2).getPeso(), 15.0);
	 assertEquals(camino.darElemento(2).getLongitud(), 3);
	 
 }
 public void testDarCaminoBFS5(){
	 setupEscenario1();
	 ListaEncadenada<NodoCamino<Integer>> camino = grafoPrueba.DarCaminoBFS(2, 5);
	 
	 assertEquals(camino.darElemento(0).getIdOrigen(), new Integer(2));
	 assertEquals(camino.darElemento(0).getIdFinal(), new Integer(5));
	 assertEquals(camino.darElemento(0).getIdAdy(), new Integer(2));
	 assertEquals(camino.darElemento(0).getPeso(), 12.0);
	 assertEquals(camino.darElemento(0).getLongitud(), 3);
	 
	 assertEquals(camino.darElemento(1).getIdOrigen(), new Integer(2));
	 assertEquals(camino.darElemento(1).getIdFinal(), new Integer(5));
	 assertEquals(camino.darElemento(1).getIdAdy(), new Integer(9));
	 assertEquals(camino.darElemento(1).getPeso(), 12.0);
	 assertEquals(camino.darElemento(1).getLongitud(), 3);
	 
	 assertEquals(camino.darElemento(2).getIdOrigen(), new Integer(2));
	 assertEquals(camino.darElemento(2).getIdFinal(), new Integer(5));
	 assertEquals(camino.darElemento(2).getIdAdy(), new Integer(1));
	 assertEquals(camino.darElemento(2).getPeso(), 12.0);
	 assertEquals(camino.darElemento(2).getLongitud(), 3);

 }
 

}
